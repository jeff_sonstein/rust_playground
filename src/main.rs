use std::env;

fn main() {
  let args: Vec< String > = env::args().collect();
  println!( "{:?}", args );
  println!( "commandline arguments:" );
  let mut counter = 0;
  for my_arg in args {
    println!( "  arg[ {} ] {:?}", counter, my_arg );
    counter = counter + 1;
  }
  println!( "environmental variables:" );
  for ( key, value ) in env::vars() {
    println!( "  [ {} ] {}", key, value );
  }
}

